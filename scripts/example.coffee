{Wit} = require 'node-wit'
oauth = require('oauth').OAuth
{TextMessage, Message, TextListener, Listener, Robot} = require 'hubot'

class ProcessedTextMessage extends TextMessage
  constructor: (@user, @text, @id) ->
    super(@user,@text,@id)

class CommandMessage extends TextMessage
  constructor: (@user, @text, @id) ->
    super(@user,@text,@id)

class IntentMessage extends Message
  constructor: (@user, @text, @intent, @entities, @id) ->
    super @user

  match: (regex) ->
    @intent.match regex

class IntentListener extends Listener
  constructor: (@robot, @regex, @options, @callback) ->
    @matcher = (message) =>
      if message.constructor.name is "IntentMessage"
        message.match @regex
    super @robot, @matcher, @options, @callback

Robot.prototype.understand = (matcher, options, callback)->
  @listeners.push new IntentListener(@, matcher, options, callback)

module.exports = (robot) ->
  robot.wit = new Wit {
    accessToken: process.env.WIT_AI_TOKEN
  }

  robot.sendSticker = (res,sticker) ->
    robot.emit 'telegram:invoke','sendSticker',
      chat_id: res.message.room
      sticker: sticker
  
  # Don't parse command messages
  robot.receiveMiddleware (context, next, done) ->
    message = context.response.message
    if message.constructor.name is "TextMessage"
      if message.text[0] is '/'
        robot.receive new CommandMessage(user, message.text)
        message.finish()
        done()
      else
        next(done)
    else
      next(done)


  # Split multiline messages as separate messages
  robot.receiveMiddleware (context, next, done) ->
    if context.response.message.constructor.name is "TextMessage"
      console.log("middleware process: ",context.response.message)
      messages = context.response.message.text.split('\n')
      console.log("messages: ",messages)
      user = context.response.message.user
      if messages.length > 1
        for text in messages
          message = new TextMessage(user, text, "message_id")
          robot.receive message
        context.response.message.finish()
        done()
      else
        next(done)
    else
      next(done)

  robot.receiveMiddleware (context, next, done) ->
    message = context.response.message
    user = context.response.message.user
    if message.constructor.name is "TextMessage"
      robot.wit.message(message.text)
        .then (data)=>
          if data?.entities?.intent
            console.log("Intent detected")
            robot.receive new IntentMessage(user, message.text, data.entities.intent[0].value, data.entities, "intent_id")
          else
            robot.receive new ProcessedTextMessage(user, message.text, "processed")
          message.finish()
          done()
        .catch (error)->
          console.log("Error")
          console.log(error)
          next(done)
    else
      next(done)

  robot.hear /maomao/i, (res) ->
    res.send('Woof!')
    robot.sendSticker(res,'BQADBQADPwIAAl7ylwJahaA2i2eAigI')

  robot.hear /bill it\n((.|\n)*)/i, (res) ->
    messages = res.match[1].split('\n')
    for message in messages
      wit_query(robot, message.trim(), res)
  
  robot.hear /test/, (res)->
    res.send('123')
    
  robot.understand /start/, (res) ->
    console.log(res.message.entities)
    res.send("Start intent detected")

  robot.understand /billing/, (res) ->
    console.log(res.message.entities)
    re = ""
    try
      if res.message.entities.file
        re += res.message.entities.file[0].value
      if res.message.entities.activity
        re += "/"
        re += "#{res.message.entities.activity[0].value}"
      if datetimes = res.message.entities.datetime
        for datetime in datetimes
          re += "/"
          if datetime.from and datetime.to
            from = Date.parse(datetime.from.value)
            to = Date.parse(datetime.to.value)
            interval = ((to - from)/60000 - 1)/60
            hour = Math.floor(interval)
            min = Math.round((interval - hour)*60)
            if hour > 0
              re += "#{hour} hour"
            if  min > 0
              re += "#{min} minute"
      if durations = res.message.entities.duration
        re += "/"
        for duration in durations
          re += "#{duration[duration.unit]} #{duration.unit}"
      res.send(re)
    catch e
      res.send(e)
  
  robot.hear /task/, (res) ->
    console.log(res.message.user)
    if not res.message.user.trello_token
      trello_oauth = new oauth(
        'https://trello.com/1/OAuthGetRequestToken',
        'https://trello.com/1/OAuthGetAccessToken',
        'e1be569654852e164831c50b95ed3d93',
        '9609ae18fa08e691162dd7b092e6326ed5a871ae68712821f4852260bb66d038',
        '1.0',
        "http://#{process.env.SERVER_NAME}:#{process.env.PORT}/callback?user=#{res.message.user.id}",
        'HMAC-SHA1'
      )
      trello_oauth.getOAuthRequestToken (e,r,s,re) ->
        res.message.user.secret_token = s
        res.send("I don't think you connected your Trello account. Don't worry, I will bring you through the process!","Click this link to grant me access:\r https://trello.com/1/OAuthAuthorizeToken?oauth_token=#{r}&name=maomao&expiration=never&scope=read,write")
    else
      res.send("Trello connected")

  # robot.hear /badger/i, (res) ->
  #   res.send "Badgers? BADGERS? WE DON'T NEED NO STINKIN BADGERS"
  #
  # robot.respond /open the (.*) doors/i, (res) ->
  #   doorType = res.match[1]
  #   if doorType is "pod bay"
  #     res.reply "I'm afraid I can't let you do that."
  #   else
  #     res.reply "Opening #{doorType} doors"
  #
  # robot.hear /I like pie/i, (res) ->
  #   res.emote "makes a freshly baked pie"
  #
  # lulz = ['lol', 'rofl', 'lmao']
  #
  # robot.respond /lulz/i, (res) ->
  #   res.send res.random lulz
  #
  # robot.topic (res) ->
  #   res.send "#{res.message.text}? That's a Paddlin'"
  #
  #
  # enterReplies = ['Hi', 'Target Acquired', 'Firing', 'Hello friend.', 'Gotcha', 'I see you']
  # leaveReplies = ['Are you still there?', 'Target lost', 'Searching']
  #
  # robot.enter (res) ->
  #   res.send res.random enterReplies
  # robot.leave (res) ->
  #   res.send res.random leaveReplies
  #
  # answer = process.env.HUBOT_ANSWER_TO_THE_ULTIMATE_QUESTION_OF_LIFE_THE_UNIVERSE_AND_EVERYTHING
  #
  # robot.respond /what is the answer to the ultimate question of life/, (res) ->
  #   unless answer?
  #     res.send "Missing HUBOT_ANSWER_TO_THE_ULTIMATE_QUESTION_OF_LIFE_THE_UNIVERSE_AND_EVERYTHING in environment: please set and try again"
  #     return
  #   res.send "#{answer}, but what is the question?"
  #
  # robot.respond /you are a little slow/, (res) ->
  #   setTimeout () ->
  #     res.send "Who you calling 'slow'?"
  #   , 60 * 1000
  #
  # annoyIntervalId = null
  #
  # robot.respond /annoy me/, (res) ->
  #   if annoyIntervalId
  #     res.send "AAAAAAAAAAAEEEEEEEEEEEEEEEEEEEEEEEEIIIIIIIIHHHHHHHHHH"
  #     return
  #
  #   res.send "Hey, want to hear the most annoying sound in the world?"
  #   annoyIntervalId = setInterval () ->
  #     res.send "AAAAAAAAAAAEEEEEEEEEEEEEEEEEEEEEEEEIIIIIIIIHHHHHHHHHH"
  #   , 1000
  #
  # robot.respond /unannoy me/, (res) ->
  #   if annoyIntervalId
  #     res.send "GUYS, GUYS, GUYS!"
  #     clearInterval(annoyIntervalId)
  #     annoyIntervalId = null
  #   else
  #     res.send "Not annoying you right now, am I?"
  #
  #
  # robot.router.post '/hubot/chatsecrets/:room', (req, res) ->
  #   room   = req.params.room
  #   data   = JSON.parse req.body.payload
  #   secret = data.secret
  #
  #   robot.messageRoom room, "I have a secret: #{secret}"
  #
  #   res.send 'OK'
  #
  # robot.error (err, res) ->
  #   robot.logger.error "DOES NOT COMPUTE"
  #
  #   if res?
  #     res.reply "DOES NOT COMPUTE"
  #
  # robot.respond /have a soda/i, (res) ->
  #   # Get number of sodas had (coerced to a number).
  #   sodasHad = robot.brain.get('totalSodas') * 1 or 0
  #
  #   if sodasHad > 4
  #     res.reply "I'm too fizzy.."
  #
  #   else
  #     res.reply 'Sure!'
  #
  #     robot.brain.set 'totalSodas', sodasHad+1
  #
  # robot.respond /sleep it off/i, (res) ->
  #   robot.brain.set 'totalSodas', 0
  #   res.reply 'zzzzz'
