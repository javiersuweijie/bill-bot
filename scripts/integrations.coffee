oauth = require('oauth').OAuth
module.exports = (robot) ->
  robot.router.get '/callback', (req,res) ->
    console.log(req.query)
    user = robot.brain.userForId(req.query.user)
    console.log(user)
    trello_oauth = new oauth(
      'https://trello.com/1/OAuthGetRequestToken',
      'https://trello.com/1/OAuthGetAccessToken',
      'e1be569654852e164831c50b95ed3d93',
      '9609ae18fa08e691162dd7b092e6326ed5a871ae68712821f4852260bb66d038',
      '1.0',
      "http://#{process.env.SERVER_NAME}:#{process.env.PORT}/callback?user=#{user.id}",
      'HMAC-SHA1'
    )
    trello_oauth.getOAuthAccessToken req.query.oauth_token, user.secret_token, req.query.oauth_verifier, (e, access_token, access_token_secret) ->
      user.access_token = access_token
      user.access_token_secret = access_token_secret
      robot.emit 'telegram:invoke', 'sendMessage', {
        chat_id: user.room
        text: 'Integration is done'
      }, ()->
        console.log(user.room)

    res.send 'OK'
